import unittest

class TestSuit(unittest.TestCase):
  def test_check2times2(self):
    self.assertEqual(2*2, 4)

  def test_checkDivision(self):
    self.assertNotEqual(7/3.0, 2)


if __name__ == '__main__':
    unittest.main()